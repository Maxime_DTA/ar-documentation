# setup.py
from setuptools import setup, find_packages

setup(
    name='myproject',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'otherproject',
    ],
    include_package_data=True,
)
