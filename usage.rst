# docs/source/usage.rst
Usage
=====

.. code-block:: python

    from myproject import MyClass
    obj = MyClass()
    obj.do_something()
