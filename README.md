[![forthebadge](https://forthebadge.com/images/badges/made-with-vue.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com)

## VEETAMINE - Dashboard
VEETAMINE is a start-up ready to boost your vending, HoReCa and Foodservice operations. For us it doesn’t matter which equipment brand you’re using neither in which utilization context. We believe in transparency and trustful communication and not in silos, closed ecosystems or locked in business models. Our team has a long term industry background from equipment operations, IT management and financial perspective and knows the pains of managing large equipment fleets efficiently and transparently.

## Technologies
- Vue3 JS 
- Pinia 
- Leaflet
- Bootstrap 5.2

## Authors 
- Maxime Dénevaud
- Joanna Baranowska
- Flavio Moreira


## Contribute to the project
VEETAMINE is an open source project. Feel free to fork the source and contribute with your own features.
